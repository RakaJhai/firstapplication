import React from 'react';
import axios from 'axios';
import './App.css'

export default class App extends React.Component{
  constructor(props){
      super(props);
      this.state = {
        listNews: [],
      }
  }

  componentDidMount(){ //otomatis terpanggil dan hanya sekali
      //fetch data from url
      axios({
          methode: 'GET',
          url: 'http://newsapi.org/v2/everything?q=apple&from=2020-11-12&to=2020-11-12&sortBy=popularity&apiKey=e81733d2d73b445cbd47e4f5581a1ce0'
      }).then(res =>{
        console.log('res', res.data.articles);
        this.setState({listNews: res.data.articles});
      }).catch(err =>{

      })
  }

  render(){
      return (
        <div>
            
            <div className="container">
                <div className="header">News For You</div>
                {this.state.listNews.map((news, index) => (
                    // <div className="container">
                        <div className="item" key={index}>
                            <div className="item-img">
                                <img src={news.urlToImage}></img>
                            </div>
                            <div className="item-content">
                                <div id="title">{news.title}</div>
                                <div id="author"><i className="fa fa-check-circle-o">{news.author}</i> <i className="fa fa-check-circle-o">{news.publishedAt}</i></div>
                                <div id="desc">{news.description}</div>
                            </div>
                        </div>
                    // </div>
                ))}
            </div>
        </div>

      );
  }
}